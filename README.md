## *Lachancea meyersii* CBS 8951

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB12931](https://www.ebi.ac.uk/ena/browser/view/PRJEB12931)
* **Assembly accession**: [GCA_900074715.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900074715.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: LAME0
* **Assembly length**: 11,261,819
* **#Chromosomes**: 8
* **Mitochondiral**: No
* **N50 (L50)**: 2,013,154 (3)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 5088
* **Pseudogene count**: 90
* **tRNA count**: 208
* **rRNA count**: 9
* **Mobile element count**: 15
