# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-05-18)

### Edited

* Delete all standard_name qualifiers.

## v1.1 (2021-05-17)

### Edited

* Build feature hierarchy.

### Fixed

* Delete a spurious CDS: LAME_0E09010G.
* LTR feature on the wrong strand: LAME_0D10704T.
* Too large frameshift: LAME_0F10506G.

## v1.0 (2021-05-17)

### Added

* The 8 annotated chromosomes of Lachancea meyersii CBS 8951 (source EBI, [GCA_900074715.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900074715.1)).
